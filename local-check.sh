#!/bin/bash

ImageName="cyancor/xserver-base"

docker build -t "$ImageName" .

docker run --net=host -e "DISPLAY" -it "$ImageName" /prepare-x.sh
Result=$?
if [ "$Result" == "0" ]; then
    printf "\n\033[1;32mXServer-Check passed.\033[0m\n\n"
    exit 0
else
    printf "\n\033[1;31mXServer-Check failed.\033[0m\n\n"
    exit $Result
fi