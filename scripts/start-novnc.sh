#!/bin/bash

VncRunning=""
while [ VncRunning == "" ]
do
    echo "Waiting for VNC server..."
    sleep 1
    VncRunning=$(lsof -i -P -n | grep 5901 | grep LISTEN)
done
echo "VNC is running."
echo "Starting noVNC..."
pushd /opt/noVNC
    ./utils/launch.sh --vnc 127.0.0.1:5901 --listen 6080 & disown
popd
