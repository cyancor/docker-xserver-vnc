#!/bin/bash

docker run -t -v $(pwd)/scripts:/wd -w /wd ubuntu:bionic /bin/bash -c \
" \
apt-get update \
&& apt-get install --yes \
    g++ \
    libx11-dev \
&& printf '#include <stdlib.h>\n#include <X11/Xlib.h>\nint main() { exit(XOpenDisplay(NULL) ? 0 : 1); }\n' > xprobe.cpp \
&& gcc -o xprobe xprobe.cpp -L/usr/X11R6/lib -lX11 \
&& chmod 0777 xprobe \
&& rm xprobe.cpp \
"
