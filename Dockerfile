FROM ubuntu:focal
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

EXPOSE 5901
EXPOSE 6080

ENV SHELL="/bin/bash" \
    DEBIAN_FRONTEND="noninteractive" \
    PATH="$PATH:/usr/games" \
    USER="root"

COPY scripts/* /
COPY configurations/background.jpg /usr/share/backgrounds/background.jpg
COPY configurations/background.png /usr/share/backgrounds/background.png
COPY configurations/xfce4-panel.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
COPY configurations/xfce4-panel.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

RUN apt-get update \
    && apt-get install --yes \
        # Base packages
        cowsay \
        ssh \
        git \
        curl \
        wget \
        htop \
        locales \
        python3 \
        openjdk-8-jdk \
        avahi-daemon \
        avahi-discover \
        avahi-utils \
        libnss-mdns \
        mdns-scan \
        telnet \
        iputils-ping \
        lsof \
        # UI
        xfce4 \
        xfce4-goodies \
        # VNC
        tightvncserver \
        # Addons
        python-numpy \ 
# Initial configuration
    && printf "\n\033[0;36mInitial configuration...\033[0m\n" \
    && locale-gen en_US.UTF-8 \
    && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 \
    && export LANG=en_US.UTF-8 \
# VNC
    && printf "\n\033[0;36mConfiguring VNC...\033[0m\n" \
    && mkdir ~/.vnc \
    && echo "12345678" | vncpasswd -f >> ~/.vnc/passwd \
    && chmod 600 ~/.vnc/passwd \
    && printf '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\nstartxfce4 &' > ~/.vnc/xstartup \
    && chmod +x ~/.vnc/xstartup \
# noVNC
    && printf "\n\033[0;36mInstalling noVNC...\033[0m\n" \
    && cd /opt \
    && git clone https://github.com/kanaka/noVNC \
    && cd /opt/noVNC/utils \
    && git clone https://github.com/novnc/websockify \
    && cp /opt/noVNC/vnc_lite.html /opt/noVNC/index.html \
    && sed -i -e 's/prompt(\"Password Required:\")/\"12345678\"/g' /opt/noVNC/index.html \
# Permissions
    && printf "\n\033[0;36mSetting permissions...\033[0m\n" \
    && chmod +x /*.sh \
# Various configurations
    && printf "\n\033[0;36mVarious configurations...\033[0m\n" \
    && echo "alias python=python3" >> ~/.bashrc \
    && printf '#!/bin/bash\npython3 "$@"\n' > /usr/bin/python && chmod +x /usr/bin/python \
    && touch /root/.Xauthority \
    && update-alternatives --set x-terminal-emulator /usr/bin/xfce4-terminal.wrapper \
    && rm -f /usr/share/backgrounds/xfce/*.jpg \
    && rm -f /usr/share/backgrounds/xfce/*.png \
    && mv /usr/share/backgrounds/background.jpg /usr/share/backgrounds/xfce/background.jpg  \
    && mv /usr/share/backgrounds/background.png /usr/share/backgrounds/xfce/background.png  \
    && ln /usr/share/backgrounds/xfce/background.jpg /usr/share/backgrounds/xfce/xfce-blue.jpg \
    && ln /usr/share/backgrounds/xfce/background.jpg /usr/share/backgrounds/xfce/xfce-teal.jpg \
    && ln /usr/share/backgrounds/xfce/background.png /usr/share/backgrounds/xfce/xfce-stripes.png \
    && rm -f /usr/share/backgrounds/*.* \
# Cleanup
    && printf "\n\033[0;36mCleanup...\033[0m\n" \
    && apt-get purge -y pm-utils xscreensaver* \
    && rm -rf /var/lib/apt/lists/* \
# Done
    && cowsay Done.

CMD [ "/run.sh" ]